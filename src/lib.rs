use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

pub struct Board(pub [[i8; 8]; 8]);

impl Board {
    pub fn new() -> Self {
        Self([[0; 8]; 8])
    }

    pub fn set(&mut self, x: usize, y: usize, a: i8) {
        self.0[y][x] = a;
    }
}

#[wasm_bindgen]
pub struct Move {
    pub x0: u8,
    pub y0: u8,
    pub x1: u8,
    pub y1: u8
}

#[wasm_bindgen]
pub fn calc_next_move(a: Vec<i8>) -> Move {
    let mut board = Board::new();
    for i in 0..64 { board.set(i % 8, i / 8, a[i]); }

    Move { x0: 1, y0: 1, x1: 1, y1: 3 }
}
